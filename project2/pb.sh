#!/bin/bash

 ansible-playbook  pb.yaml \
 -i inventories/dev/hosts \
 -vv \
 -c local \
 -K \
 --vault-password-file vault_pass.txt \

